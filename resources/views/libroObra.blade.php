<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <title>Creación de Proyectos</title>
    
     <!-- Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
     <body style="background-color:#6195ED">  
</head>
<body>
    <div class="container">
    <div class="table-wrapper">
            <div class="table-title">
                <br>
                <div class="row">
                    <div class="col-sm-8"><h2>Creación de <b>Libro de Obra</b></h2></div>
                    <div class="col-sm-4">                        
                        <a href="/CAM/public" class="btn btn-info add-new"><i class="fa fa-arrow-left"></i> Ir a Inicio</a>                        
                    </div>
                </div>                
    </div>
        <div class="row">
            <div class="col-md-5">
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="Fecha de inicio">Fecha del Evento</label>
                        <input type="date" name="Fecha del Evento" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Tipo de Evento:</label>
                        <input type="text" name="tipo evento" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Notas del evento:</label>
                        <hr>
                        <textarea name="notas" id="" cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                    <label class="col-md-4 control-label">Adjuntar Archivo</label>
                    <div class="col-md-6">
                        <input type="file" class="form-control" name="file" >
                    </div>
                    </div>
                    <div class="col-md-12 pull-right">
				    <hr>
					<button type="submit" class="btn btn-success">Guardar datos</button>
				    </div>                
                </form>
            </div>
        </div>
    </div>
</body>
</html>