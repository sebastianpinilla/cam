<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Creación de Proyectos</title>
    
     <!-- Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
     <body style="background-color:#6195ED">  
</head>
<body>
    <div class="container">
    <br>
    <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Creación de <b>Proyectos</b></h2></div>                   
                    <div class="col-sm-4">
                        <a href="libroObra" class="btn btn-info add-new"><i class="fa fa-arrow-left"></i> Ir a Libro Obra</a>                    
                    </div>
                    <div class="form-group">
                    <label class="col-md-4 control-label">Cargar Imagenes</label>
                    <div class="col-md-6">
                        <input type="file" class="form-control" name="file" >
                    </div>
                    </div>
                </div>
    </div>
        <div class="row">
            <div class="col-md-5">
                <form method="POST">
                    <div class="form-group">
                        <label for="Nombre">Nombre</label>
                        <input type="text" name="Nombre" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="Fecha de inicio">Fecha de inicio</label>
                        <input type="date" name="Fecha de inicio" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Fecha estimada de cierre</label>
                        <input type="date" name="Fecha estimada de cierre" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Responsable</label>
                        <input type="text" name="Responsable" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Clase de Proyecto</label>
                        <br>
                        <select name="Clase de Proyecto" class="form-group">
                            <option value="Obras Electricas">Obras Electricas</option>
                            <option value="Obras civiles">Obras civiles</option>
                            <option value="Mantenimiento">Mantenimiento</option>
                        </select>
                    </div>
                    <label>Estado del Proyecto:</label>
                    <div class="form-group">                        
                        <label>Activo</label>
                        <input type="radio" name="Activo" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Inactivo</label>
                        <input type="radio" name="Inactivo" class="form-control">
                    </div>
                    <div class="col-md-12 pull-right">
				    <hr>
					<button type="submit" class="btn btn-success">Guardar datos</button>
				</div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>